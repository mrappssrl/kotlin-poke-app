package com.mirkopruiti.kotlinpokemontest.repository

import android.content.Context
import com.mirkopruiti.kotlinpokemontest.data.api.ApiCall
import com.mirkopruiti.kotlinpokemontest.data.db.PokemonDao
import com.mirkopruiti.kotlinpokemontest.data.model.PokemonInfo
import com.mirkopruiti.kotlinpokemontest.util.NetworkUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class DetailsRepository (private val apiCall: ApiCall, private val pokemonDao: PokemonDao) {

    suspend fun getPokemonInfo(name: String, context: Context): PokemonInfo {
        return try {
            val pokemon = apiCall.fetchPokemonInfo(name).body()!!
            pokemonDao.insertPokemonInfo(pokemon)
            pokemon
        } catch (t: Throwable) {
            pokemonDao.getPokemonInfo(name) ?: throw t
        }
    }

}